/**
 * View Models used by Spring MVC REST controllers.
 */
package com.mck.bank.api.web.rest.vm;
