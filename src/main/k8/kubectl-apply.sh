#!/bin/bash
# Files are ordered in proper order with needed wait for the dependent custom resource definitions to get initialized.
# Usage: bash kubectl-apply.sh

kubectl apply -f ./src/main/k8/registry/
kubectl apply -f ./src/main/k8/mybank/
